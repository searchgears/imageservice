# REST API image fetching

 Die REST Schnittstelle stellt einen Endpunkt zur Verfügung dem eine oder mehrere Urls zu Bildern übergeben werden.
 Die angegebenen Bilder werden abgeholt. Falls nötig wird ein Thumbnail erstellt (die Größe wird angepasst).
 Das Ergebnis wird als base64 enkodiertes Bild zurückgegeben.

 Der Endpunkt wird über eine POST url aufgerufen, die ein oder mehrer urls im request body bekommt.



# Installation

##Voraussetzung:
python3.4
pip3

pip3 install -r requirements.txt


Alternativ zu falcon (http://falconframework.org/) kann auch eine andere python REST library verwendet werden.


TODO:
FIX tests
FIX logging (aufruf der Logging methode falsch)