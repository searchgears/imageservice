import base64, logging
from io import BytesIO
from PIL import Image
import requests


XPATH_FOR_FOLLOWING_IMAGES = "following::*[position() <12][local-name() = 'img']/@src"


def create_image_from_content(content):
    return Image.open(BytesIO(content))

def get_base64_encoded_image(raw_image):
    return "data:image/jpeg;base64," + base64.b64encode(raw_image)


def save_image_as_jpeg(image, as_thumbnail=False): #TODO: test
    converted_image = image.convert('RGB')
    if as_thumbnail:
        converted_image.thumbnail((100, 100), Image.ANTIALIAS)
    output = BytesIO()
    converted_image.save(output, format='JPEG')
    raw_image = output.getvalue()
    return raw_image


class ImageExtractor(object):

    def __init__(self, user_agent):
        self.user_agent = user_agent

    def create_base64_encoded_image(self, image_url, check_format=True):
        raw_image = self.create_image_if_valid(image_url, check_format)
        return self.get_base64_encoded_image(raw_image)

    def create_image_if_valid(self, image_url, check_format=True): #TODO as optional arguments min and max size
        try:
            image = self._create_image(image_url)
        except Exception as exx:
            if len(image_url) < 2000:  # TODO handle base64 image
                logging.info(
                        format="An error occurred getting the image '%(image_url)s' at url '%(url)s'. Reason:%(reason)s",
                        image_url=image_url, reason=exx)
            else:
                logging.info(
                        format="An error occurred getting a potential base64image. Reason:%(reason)s",
                        reason=exx)
            return None

        try:
            if not check_format or self._image_format_is_valid(image):
                raw_image = save_image_as_jpeg(image, as_thumbnail=True)
                self._raise_if_image_file_size_is_too_small(raw_image, image_url)
                return raw_image
        except Exception as exx:
            logging.info(
                    format="An error occurred saving the image '%(image_url)s'. Reason:%(reason)s",
                    image_url=image_url,
                    reason=exx)
        finally:
            image.close()
        return None


    def _image_format_is_valid(self, image):
        return image.width > 200 and image.height > 100 and not self._has_ad_format(
            image) and not self._has_extreme_width_height_ratio(image)

    def _create_image(self, image_url):
        image_response = requests.get(image_url, headers={'User-Agent': self.user_agent})
        return create_image_from_content(image_response.content)

    def _has_extreme_width_height_ratio(self, image):
        return image.width / float(image.height) > 2.5 or image.height / float(image.width) > 2.5

    def _image_url_is_relative(self, image_url):
        return (image_url.startswith("/") or image_url.startswith(".")) or (not image_url.startswith("www.") and not image_url.startswith("http://"))

    def _raise_if_image_file_size_is_too_small(self, raw_image, image_url):
        if len(raw_image) < 1300:
            raise ValueError("The file size of the image at %s is too small. Is it a fake image?" %image_url)

    def _has_ad_format(self, image):
        return image.width == 300 and image.height == 250

