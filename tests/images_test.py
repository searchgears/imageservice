import unittest
import requests_mock
from images import ImageExtractor

IMAGE_FILE_WITH_EXTREME_WIDTH = 'invalid_image_banner.jpg'
IMAGE_FILE_WITH_EXTREME_HEIGHT = 'invalid_image_skyscraper.jpg'
IMAGE_FILE_WITH_LITTLE_WIDTH = 'invalid_image_200_190.jpg'
IMAGE_FILE_WITH_LITTLE_HEIGHT = 'invalid_image_300_99.jpg'
FAKE_IMAGE_FILE = 'fake_image.gif'
DEFAULT_VALID_TEST_IMAGE_FILE = 'valid_image_400_200.jpg'
DEFAULT_TEST_IMAGE_URL = 'http://www.schnu.de/valid_image.png'

USERAGENT = "useragent"
HEADERS = {'User-Agent': USERAGENT}

class ImageExtractorTest(unittest.TestCase):

    def test_image(self):
        with requests_mock.mock() as m:
            m.get(u'http://baseurl.de/valid_image.png', content=self._get_test_image(DEFAULT_VALID_TEST_IMAGE_FILE), headers=HEADERS)
            image = self._get_image("http://baseurl.de/valid_image.png")
            self.assert_image_valid(image)

    def test_ignore_first_fake_image(self):
        self.assert_that_image_is_not_extracted(FAKE_IMAGE_FILE)

    def test_ignore_image_because_of_height(self):
        self.assert_that_image_is_not_extracted(IMAGE_FILE_WITH_LITTLE_HEIGHT)

    def test_ignore_image_because_of_width(self):
        self.assert_that_image_is_not_extracted(IMAGE_FILE_WITH_LITTLE_WIDTH)

    def test_ignore_image_in_ad_format(self):
        IMAGE_FILE_AD_FORMAT = 'invalid_image_300_250.jpg'
        self.assert_that_image_is_not_extracted(IMAGE_FILE_AD_FORMAT)

    def test_ignore_skyscraper_image(self):
        self.assert_that_image_is_not_extracted(IMAGE_FILE_WITH_EXTREME_HEIGHT)

    def test_ignore_banner_image(self):
        self.assert_that_image_is_not_extracted(IMAGE_FILE_WITH_EXTREME_WIDTH)

    def test_ignore_first_image_in_wrong_format(self):# TODO: exception statt silent failure? #self.assertRaisesRegexp(IOError, ".*", extractor.extract_article_image)
        with requests_mock.mock() as m:
            m.get(DEFAULT_TEST_IMAGE_URL, content='noImage', headers=HEADERS)
            image = self._extract_image(DEFAULT_TEST_IMAGE_URL)
            self.assertIsNone(image)

    def _get_test_image(self, filename):
        a =  open("images/" + filename, 'rb').read()
        return a

    def assert_image_valid(self, image):
        self.assertIsNotNone(image)
        self.assertLess(1300, len(image))

    def assert_that_image_is_not_extracted(self, image_filename):
        self.assert_that_image_is_not_extracted_for_url("http://www.schnu.de/valid_image.png", image_filename)

    def assert_that_image_is_not_extracted_for_url(self, url, image_filename):
        with requests_mock.mock() as m:
            self._mock_image_for_default_url(image_filename, m)
            image = self._get_image(url)
            self.assertIsNone(image)

    def assert_that_image_is_extracted(self, url, image_filename):
        with requests_mock.mock() as m:
            self._mock_image_for_default_url(image_filename, m)
            image = self._get_image(url)
            self.assert_image_valid(image)

    def _mock_image_for_default_url(self, image_filename, mock):
        mock.get(DEFAULT_TEST_IMAGE_URL, content=self._get_test_image(image_filename), headers=HEADERS)

    def _mock_image(self, image_url, image_filename, mock):
        mock.get(image_url, content=self._get_test_image(image_filename), headers=HEADERS)

    def _get_image(self, url):
        extractor = ImageExtractor("useragent")
        image = extractor.create_image_if_valid(url)
        return image

    #TODO: test base64 method!!